# CMake generated Testfile for 
# Source directory: /home/autoware/shared_dir/catkin_ws/src/sdb_pkg
# Build directory: /home/autoware/shared_dir/catkin_ws/build/sdb_pkg
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_sdb_pkg_gtest_utest "/home/autoware/shared_dir/catkin_ws/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/autoware/shared_dir/catkin_ws/build/test_results/sdb_pkg/gtest-utest.xml" "--return-code" "/home/autoware/shared_dir/catkin_ws/devel/lib/sdb_pkg/utest --gtest_output=xml:/home/autoware/shared_dir/catkin_ws/build/test_results/sdb_pkg/gtest-utest.xml")
