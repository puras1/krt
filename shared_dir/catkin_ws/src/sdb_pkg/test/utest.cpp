// Bring in my package's API, which is what I'm testing

// Bring in gtest
	

#include "../src/test.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */


// Declare another test
TEST_F(SdmTest, testCase1)
{
   //test things here, calling EXPECT_* and/or ASSERT_* macros as needed

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);  

  pcl::PointXYZ pnts1 {2.0, 0.0, 0.0};
  pcl::PointXYZ pnts2 {8.0, 0.0, 0.0};
  pcl::PointXYZ pnts3 {6.0, 0.0, 0.0};
  pcl::PointXYZ pnts4 {1.0, 0.0, 0.0};
  pcl::PointXYZ pnts5 {3.0, 0.0, 0.0};
  pcl::PointXYZ pnts6 {9.0, 0.0, 0.0};
  pcl::PointXYZ pnts7 {7.0, 0.0, 0.0};

  cloud->points.push_back(pnts1);
  cloud->points.push_back(pnts2);
  cloud->points.push_back(pnts3);
  cloud->points.push_back(pnts4);
  cloud->points.push_back(pnts5);
  cloud->points.push_back(pnts6);
  cloud->points.push_back(pnts7);
  

  EXPECT_EQ(1, testClass.getClosestPoint(cloud));

}

TEST_F(SdmTest, testCase2)
{
   //test things here, calling EXPECT_* and/or ASSERT_* macros as needed

  EXPECT_EQ(3.5, testClass.calcStoppingDistance(7.0));

}

// Run all the tests that were declared with TEST()
int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "tester");
  //ros::NodeHandle nh;
  return RUN_ALL_TESTS();
}