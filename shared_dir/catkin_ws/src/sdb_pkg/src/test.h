#include "talker.h"
#include <gtest/gtest.h>

#ifndef TEST_H
#define TEST_H

class SdmTest : public testing::Test
{
        protected:
            Sdm_Class testClass;
            // If the constructor and destructor are not enough for setting up
            // and cleaning up each test, you can define the following methods:

            SdmTest() {
                // Code here will be called immediately after the constructor (right
                // before each test).
                // testClass = new Sdm_Class;
            }

            virtual ~SdmTest() {
                // Code here will be called immediately after each test (right
                // before the destructor).
               // delete testClass;
            }
};

#endif // TEST_H