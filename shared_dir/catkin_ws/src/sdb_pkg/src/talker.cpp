/*PURAS: Stopping Distance Monitor */

#include "talker.h"


void Sdm_Class::updateMarkers()
{
  car_markers.marker_stop.pose.position.x = 4 + stopping_distance/2;
  car_markers.marker_free_area.scale.x = free_area - 4.5;     
  car_markers.marker_free_area.pose.position.x = 2.5 + free_area/2;
  car_markers.text_marker.text = "current speed = " + std::to_string(initial_speed) + "\nstopping distance = " + std::to_string(stopping_distance);
}

Sdm_Class::Sdm_Class()
{

}

void Sdm_Class::setMarkers()
{

  //stopping distance car_markers.marker_stop
  car_markers.marker_stop.header.frame_id = "/base_link";        
  car_markers.marker_stop.header.stamp = ros::Time();
  car_markers.marker_stop.ns = "my_namespace";
  car_markers.marker_stop.id = 0;
  car_markers.marker_stop.type = visualization_msgs::Marker::CUBE;
  car_markers.marker_stop.action = visualization_msgs::Marker::ADD;
  car_markers.marker_stop.pose.position.x = 4.5;
  car_markers.marker_stop.pose.position.y = 0;
  car_markers.marker_stop.pose.position.z = 0.05;
  car_markers.marker_stop.pose.orientation.x = 0.0;
  car_markers.marker_stop.pose.orientation.y = 0.0;
  car_markers.marker_stop.pose.orientation.z = 0.0;
  car_markers.marker_stop.pose.orientation.w = 1.0;
  car_markers.marker_stop.scale.x = 7;     // length
  car_markers.marker_stop.scale.y = 1.5;   // width
  car_markers.marker_stop.scale.z = 0.1;
  car_markers.marker_stop.color.a = 1.0; 
  car_markers.marker_stop.color.r = 0.0;
  car_markers.marker_stop.color.g = 1.0;
  car_markers.marker_stop.color.b = 0.0;


  //stopping distance car_markers.marker_free_area
  car_markers.marker_free_area.header.frame_id = "/base_link";        
  car_markers.marker_free_area.header.stamp = ros::Time();
  car_markers.marker_free_area.ns = "my_namespace";
  car_markers.marker_free_area.id = 2;
  car_markers.marker_free_area.type = visualization_msgs::Marker::CUBE;
  car_markers.marker_free_area.action = visualization_msgs::Marker::ADD;
  car_markers.marker_free_area.pose.position.x = 4.5;
  car_markers.marker_free_area.pose.position.y = 0;
  car_markers.marker_free_area.pose.position.z = 0.01;
  car_markers.marker_free_area.pose.orientation.x = 0.0;
  car_markers.marker_free_area.pose.orientation.y = 0.0;
  car_markers.marker_free_area.pose.orientation.z = 0.0;
  car_markers.marker_free_area.pose.orientation.w = 1.0;
  car_markers.marker_free_area.scale.x = 7;     
  car_markers.marker_free_area.scale.y = 1.5;   
  car_markers.marker_free_area.scale.z = 0.1;
  car_markers.marker_free_area.color.a = 0.7; 
  car_markers.marker_free_area.color.r = 0.0;
  car_markers.marker_free_area.color.g = 0.0;
  car_markers.marker_free_area.color.b = 1.0;


  //car speed car_markers.text_marker
  car_markers.text_marker.header.frame_id = "/base_link";        
  car_markers.text_marker.header.stamp = ros::Time();
  car_markers.text_marker.ns = "my_namespace1";
  car_markers.text_marker.id = 1;
  car_markers.text_marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  car_markers.text_marker.action = visualization_msgs::Marker::ADD;
  car_markers.text_marker.pose.position.x = 10;
  car_markers.text_marker.pose.position.y = 1;
  car_markers.text_marker.pose.position.z = 0.05;
  car_markers.text_marker.pose.orientation.x = 0.0;
  car_markers.text_marker.pose.orientation.y = 0.0;
  car_markers.text_marker.pose.orientation.z = 0.0;
  car_markers.text_marker.pose.orientation.w = 1.0;
  car_markers.text_marker.scale.z = 2;
  car_markers.text_marker.color.a = 1.0; // Don't forget to set the alpha!
  car_markers.text_marker.color.r = 0.67;
  car_markers.text_marker.color.g = 0.84;
  car_markers.text_marker.color.b = 0.9; 
}

/* Taking estimated velocity from node */
void Sdm_Class::velocityCallback(const geometry_msgs::Vector3Stamped& est_vel)
{
   initial_speed = (initial_speed + est_vel.vector.x)/2; 
}

void Sdm_Class::filterCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out)
{
  pcl::PassThrough<pcl::PointXYZ> pass;

  pass.setInputCloud (cloud_out);
  pass.setFilterFieldName ("y");
  pass.setFilterLimits (-1.0, 1);  
  pass.filter (*cloud_out);

  pass.setInputCloud (cloud_out);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (1.5, 5);  
  pass.filter (*cloud_out);

  pass.setInputCloud (cloud_out);
  pass.setFilterFieldName ("x");
  pass.setFilterLimits (4.5, 40);  
  pass.filter (*cloud_out);
}

float Sdm_Class::getClosestPoint(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out)
{
  float curr_x = 10.0;
  for (int i = 0; i < cloud_out->points.size(); i++)
  {
      if (cloud_out->points[i].x < curr_x) 
      {
        curr_x = cloud_out->points[i].x;
      }
        
  }
  return curr_x;
}

void Sdm_Class::pointsCallback(const sensor_msgs::PointCloud2ConstPtr& pCloud)
{

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out(new pcl::PointCloud<pcl::PointXYZ>);

  pcl::fromROSMsg (*pCloud, *cloud);  // convert the cloud to pcl::PointCloud<pcl::PointXYZ>
  
  pcl_ros::transformPointCloud(*cloud, *cloud_out, transform); //change coordinate

  filterCloud(cloud_out);

  free_area = getClosestPoint(cloud_out);

  if (free_area > (4.5 + stopping_distance)) 
  {
    object_detected = false;
  }
  else 
  {
    object_detected = true;
  }  
}

void Sdm_Class::init()
{
 try
  {

    ros::Time now = ros::Time::now();
    listener.waitForTransform("/base_link", "/velodyne", now, ros::Duration(4.0));
    listener.lookupTransform("/base_link", "/velodyne", ros::Time(0), transform); 
  }
  catch (tf::TransformException ex){
    ROS_ERROR("%s",ex.what());
    ros::Duration(1.0).sleep();
  }
  
  vel_sub = node_handle.subscribe("estimated_vel", 0, &Sdm_Class::velocityCallback, this);
  points_raw = node_handle.subscribe<sensor_msgs::PointCloud2>("points_raw", 0, &Sdm_Class::pointsCallback, this);

  vis_pub = node_handle.advertise<visualization_msgs::Marker>( "visualization_marker", 1 );

}

void Sdm_Class::publishInfo()
{
    /* Publish marker & text marker*/
    vis_pub.publish( car_markers.marker_free_area );
    vis_pub.publish( car_markers.marker_stop );
    vis_pub.publish( car_markers.text_marker );
}

void Sdm_Class::changeColor()
{
    if (object_detected == true)
    {
      car_markers.marker_stop.color.r = 1.0;
      car_markers.marker_stop.color.g = 0.0;
    }
    else
    {
      car_markers.marker_stop.color.r = 0.0;
      car_markers.marker_stop.color.g = 1.0;
    }
}
float Sdm_Class::calcStoppingDistance(float init_speed)
{
    /* Calculate Stopping Distance & update text marker */
    stopping_distance = (-1 * (init_speed * init_speed)) / (2 * max_acc);
    return stopping_distance;
}

