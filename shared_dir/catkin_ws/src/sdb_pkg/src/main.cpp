
#include "talker.h"

int main(int argc, char **argv)
{

  ros::init(argc, argv, "talker");
  Sdm_Class sdm;
  sdm.init();
 
  sdm.setMarkers();

  ros::Rate loop_rate(10);

  while (ros::ok())
  {
    sdm.car_markers.marker_stop.scale.x = sdm.calcStoppingDistance(sdm.initial_speed);

    sdm.updateMarkers();

    sdm.changeColor();

    sdm.publishInfo();    

    ros::spinOnce();

    loop_rate.sleep();   
  }

  return 0;
}
