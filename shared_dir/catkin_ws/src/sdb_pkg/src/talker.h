#include "ros/ros.h"

#include "std_msgs/String.h"
#include "sensor_msgs/PointCloud2.h"
#include "geometry_msgs/Vector3Stamped.h"

#include <sstream>
#include <iostream>
#include <cmath>        // std::abs
#include <string>
#include <visualization_msgs/Marker.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/filters/passthrough.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/common/transforms.h>
#include <tf/transform_datatypes.h>

 
#ifndef TALKER_H
#define TALKER_H

struct car_marker_s{
    visualization_msgs::Marker marker_stop;
    visualization_msgs::Marker marker_free_area;
    visualization_msgs::Marker text_marker;
};

class Sdm_Class 
{

    private:
    
        const float max_acc = -7.0f; 
        ros::NodeHandle node_handle;
        tf::TransformListener listener;  
        ros::Subscriber vel_sub;
        ros::Subscriber points_raw;
        ros::Publisher vis_pub;
    
        tf::StampedTransform transform;
        bool object_detected = false;
        float stopping_distance;
        float free_area;  

    public:
        float initial_speed = 0;
        car_marker_s car_markers;
        void filterCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out);
        float getClosestPoint(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out);
        void velocityCallback(const geometry_msgs::Vector3Stamped& est_vel);
        void pointsCallback(const sensor_msgs::PointCloud2ConstPtr& pCloud);
        void setMarkers();
        void updateMarkers(); 
        void publishInfo();
        void changeColor();
        float calcStoppingDistance(float);
        void init();
        Sdm_Class();
                  
};


#endif //TALKER_H