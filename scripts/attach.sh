#!/bin/bash

DOCKER_PID=$(docker ps | grep autoware | awk '{ print $1 }')

if [ ! -z "$DOCKER_PID" ]
then
    docker exec -u autoware -w "/home/autoware" -it $DOCKER_PID /bin/bash -c "source /opt/ros/kinetic/setup.bash && bash"
else
    echo 'No running docker'
fi

